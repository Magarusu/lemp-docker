#!/bin/bash

if [ ! "$(ls -A $PROJECT_FOLDER)" ]; then
     # Pull project from git.
    if [ $GIT_REPO ]; then
        git clone $GIT_REPO
    fi

    # Pull project from composer.
    if [ $COMPOSER_REPO ]; then
        composer create-project --repository-url=$COMPOSER_REPO $PROJECT_FOLDER
    fi
elif [ -d $PROJECT_FOLDER/.git ]; then
    cd $PROJECT_FOLDER && git pull;
fi

# Apply backup.
if [ ! -f $PROJECT_FOLDER/backup.flag ]; then
    echo
    for f in /docker-entrypoint-init.d/*; do
        case "$f" in
            *.tar)      echo "$0: running $f"; tar -xvf "$f"; echo ;;
            *.tar.gz)   echo "$0: running $f"; tar -xzvf "$f"; echo ;;
            *.tar.bz2)  echo "$0: running $f"; tar -xjvf "$f"; echo ;;
            *)          echo "$0: ignoring $f" ;;
        esac
        echo
    done
    touch $PROJECT_FOLDER/backup.flag
fi

# Run extra scripts
for f in /docker-entrypoint-init.d/*.sh; do
    bash "$f";
done

# Start the php-fpm service
/usr/local/sbin/php-fpm
